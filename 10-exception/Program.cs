﻿using System;
using System.IO;

namespace _10_exception
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tab = new int[5];
            try
            {
                //
                File.OpenRead("nexistepas");      // Lance une Exception FileNotFoundException       
                tab[10] = 100;                    // Lance une Exception IndexOutOfRangeException                      
                Console.WriteLine("La suite des instructions");
            }
            catch (FileNotFoundException e)      // Attrape une Exception FileNotFoundException          
            {
                Console.WriteLine("le fichier n'existe {0}",e.Message);
            }
            catch (IndexOutOfRangeException e)  // Attrape une Exception IndexOutOfRangeException
            {
                Console.WriteLine("En dehors des limites du tableau {0}",e.GetType());
            }
            catch (Exception e)                 // Attrape tous les autres Exception
            {
                Console.WriteLine("une exception a été lancer {0}", e.GetType());   // GetType => retourne le type de l'exception
            }
            finally
            {
                Console.WriteLine("Bloc finally ");     // Le bloc finnaly est toujours éxécuter 
            }
            Console.WriteLine("La suite du programme  -1-\n");



            try
            {
                Console.Write("Saisir un age ");
                int age = Convert.ToInt32(Console.ReadLine());
                GererAge(age);
            }
            catch (AgeNegatifException e)
            {
                Console.WriteLine(e.Message + " " + e.GetType());   // Message => récupérer le messsage de l'exception
            }
            catch (Exception e)
            {
                Console.WriteLine("Erreur de saisie L'age doit être un nombre ({0})", e.GetType());
            }
            Console.WriteLine("La suite du programme  -2-\n");



            try
            {
                LireFichier("existepas");
            }
            catch (Exception e)
            {
                Console.WriteLine("une exception a été lancer (traitement de l'utilisateur de la fonction)\n\t{0}\n\t{1}", e.GetType(), e.InnerException.Message);
            }   // InnerException => exception qui est à l'origine de cette exception
            Console.WriteLine("La suite du programme  -3-\n");


            Console.ReadKey();
        }

        static void GererAge(int age)
        {
            if (age < 0)
            {
                throw new AgeNegatifException("age négatif");     //  throw => Lancer un exception
            }
            Console.WriteLine("traitement GererAge");
        }

        static void LireFichier(string path)
        {
            FileStream sf = null;
            try
            {
                sf = File.OpenRead(path);
            }
            catch (FileNotFoundException e)                     // Traitement partiel  de l'exception FileNotFoundException 
            {
                Console.WriteLine("le fichier n'existe (traitement local à la fonction)");
                // throw e;                                     // On relance l'exception pour que l'utilisateur de la méthode la traite a son niveau
                throw new Exception("Erreur Lirefichier", e);   // On relance une autre exception ,e =>référence à l'exception interne qui a provoquer l'exception
            }
            finally
            {
                if (sf != null)
                {
                    sf.Close();
                    sf.Dispose();
                }
            }
        }


    }
}
