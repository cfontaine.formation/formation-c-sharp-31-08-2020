﻿using System;

namespace _10_exception
{
    // On peut créer ses propres exceptions en héritant de la classe Exception ou ApplicationException
    // Par convention, toutes les sous-classes ont un nom se terminant par Exception

    class AgeNegatifException : ApplicationException
    {
        public AgeNegatifException()
        {

        }

        public AgeNegatifException(string msg) : base(msg)  // base => appel au constructeur de la classe Exception
        {

        }
        public AgeNegatifException(string msg, Exception innerException) : base(msg, innerException)
        {

        }

    }
}
