﻿using System;

namespace _06_Poo
{
    class Banque
    {
        private CompteBancaireP[] comptes = new CompteBancaireP[5];
        private Personne Directeur { get; set; }

        public Banque(Personne directeur)
        {
            this.Directeur = directeur;
        }

        //Indexeur
        public CompteBancaireP this[string index]
        {
            get
            {
                if (Convert.ToInt32(index) > comptes.Length)
                {
                    throw new IndexOutOfRangeException();
                }
                else
                {
                    return comptes[Convert.ToInt32(index)];
                }
            }
            set
            {
                if (Convert.ToInt32(index) > comptes.Length)
                {
                    throw new IndexOutOfRangeException();
                }
                else
                {
                    comptes[Convert.ToInt32(index)] = value;
                }
            }
        }

    }
}
