﻿namespace _06_Poo
{
    class Proprietaire
    {
        public string nom;
        public string prenom;

        public Proprietaire()
        {
        }

        public Proprietaire(string nom, string prenom)
        {
            this.nom = nom;
            this.prenom = prenom;
        }
    }
}
