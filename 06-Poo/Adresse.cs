﻿namespace _06_Poo
{
    class Adresse
    {
        public string Rue { get; set; }
        public string Ville { get; set; }
        public int CodePostal { get; set; }

        public Adresse(string rue, string ville, int codePostal)
        {
            Rue = rue;
            Ville = ville;
            CodePostal = codePostal;
        }
    }
}
