﻿namespace _06_Poo
{
    class Cercle
    {
        public Point centre;
        public double rayon;

        public Cercle()
        {
        }

        public Cercle(Point centre, double rayon)
        {
            this.centre = centre;
            this.rayon = rayon;
        }

        public static bool Collision(Cercle c1, Cercle c2)
        {
            return Point.DistanceXY(c1.centre, c2.centre) <= c1.rayon + c2.rayon;
        }

        public bool Inclusion(Point p)
        {
            return Point.DistanceXY(centre, p) < rayon;
        }
    }
}
