﻿namespace _06_Poo
{
    class Personne
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }
        public Adresse Adresse { get; set; }

        public Personne(string nom, string prenom, string email, Adresse adresse)
        {
            Nom = nom;
            Prenom = prenom;
            Email = email;
            Adresse = adresse;
        }
    }
}
