﻿using System;

namespace _06_Poo
{
    class CompteBancaireP
    {
        // protected La méthode set n'est accéssible que pour les classes enfants
        public double Solde { get; protected set; } = 50; // Valeur par défaut à partir de C# 6
        public string Iban { get; set; }
        public Personne Titulaire { get; set; }

        // Compteur de compte créé
        public static int compteur;
        public CompteBancaireP()
        {
            compteur++;
            Iban = "fr-5962-0000-" + compteur;
        }

        // this permet de chainer les constructeurs
        // Ce constructeur, appelle le constructeur par défaut 
        public CompteBancaireP(Personne titulaire) : this()
        {
            //Solde = 50;  // Valeur par défaut pour une version inférieur à C# 6
            Titulaire = titulaire;
        }

        public CompteBancaireP(double solde, Personne titulaire) : this(titulaire)
        {
            Solde = solde;
        }

        public void Afficher()
        {
            Console.WriteLine("_________________________________");
            Console.WriteLine("Solde={0}", Solde);
            Console.WriteLine("Iban={0}", Iban);
            Console.WriteLine("Titulaire={0} {1}", Titulaire.Prenom, Titulaire.Nom);
            Console.WriteLine("_________________________________");
        }

        public void Crediter(double valeur)
        {
            if (valeur > 0)
            {
                Solde += valeur;
            }
        }

        public void Debiter(double valeur)
        {
            if (valeur > 0)
            {
                Solde -= valeur;
            }
        }

        public bool EstPositif()
        {
            return Solde > 0;
        }
    }

}
