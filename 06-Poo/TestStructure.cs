﻿using System;

namespace _06_Poo
{
    struct TestStructure
    {
        public int data;

        //public TestStructure() //
        //{

        //}
        public TestStructure(int data)
        {
            this.data = data;
        }

        public void ModifData(int data)
        {
            this.data = data;
        }

        public void Afficher()
        {
            Console.WriteLine(data);
        }
    }
}
