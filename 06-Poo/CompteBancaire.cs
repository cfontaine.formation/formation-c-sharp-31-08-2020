﻿using System;

namespace _06_Poo
{
    class CompteBancaire
    {
        // Propriétés
        public double solde = 50.0;
        public string iban;
        public string titulaire;

        public static int compteur;
        public CompteBancaire()
        {
            compteur++;
            iban = "fr-5962-0000-" + compteur;
        }

        public CompteBancaire(string titulaire) : this()
        {
            //compteur++;
            //iban = "fr-5962-0000-" + compteur;
            this.titulaire = titulaire;
        }

        public CompteBancaire(double solde, string titulaire) : this(titulaire)
        {
            //compteur++;
            //iban = "fr-5962-0000-" + compteur;
            this.solde = solde;
            // this.titulaire = titulaire;
        }

        public void Afficher()
        {
            Console.WriteLine("_________________________________");
            Console.WriteLine("Solde={0}", solde);
            Console.WriteLine("Iban={0}", iban);
            Console.WriteLine("Titulaire={0}", titulaire);
            Console.WriteLine("_________________________________");
        }

        public void Crediter(double valeur)
        {
            if (valeur > 0)
            {
                solde += valeur;
            }
        }

        public void Debiter(double valeur)
        {
            if (valeur > 0)
            {
                solde -= valeur;
            }
        }

        public bool EstPositif()
        {
            return solde > 0;
        }
    }
}
