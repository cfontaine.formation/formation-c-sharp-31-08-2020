﻿namespace _06_Poo
{
    static class Tools
    {
        public static bool IsNotnull(string str)
        {
            return str != null;
        }

        public static bool IsSizeMin(string str, int sizeMin)
        {
            return str.Length > sizeMin;
        }

    }
}
