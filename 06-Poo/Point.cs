﻿using System;

namespace _06_Poo
{
    class Point
    {
        public double x;
        public double y;

        public Point()
        {
        }

        public Point(double xc, double yc)
        {
            x = xc;
            y = yc;
        }

        public void Afficher()
        {
            Console.WriteLine("({0},{1})", x, y);
        }

        public void Deplacer(double tx, double ty)
        {
            x += tx;
            y += ty;
        }

        public double Norme()
        {
            return Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
        }

        public static double DistanceXY(Point pa, Point pb)
        {
            return Math.Sqrt(Math.Pow((pb.x - pa.x), 2) + Math.Pow((pb.y - pa.y), 2));
        }
    }
}
