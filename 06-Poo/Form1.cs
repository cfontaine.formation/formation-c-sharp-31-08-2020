﻿using System;

namespace _06_Poo
{
    // classe partielle => définie sur plusieurs fichiers
    public partial class Form1
    {
        private int test;

        public void Calcul()
        {
            Console.WriteLine("Calcul");
        }
    }
}
