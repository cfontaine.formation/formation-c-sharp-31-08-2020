﻿using System;

namespace _06_Poo
{
    class Program
    {
        static void Main(string[] args)
        {
            Voiture.TestMethodeClasse();
            Console.WriteLine(Voiture.compteurVoiture);
            Voiture v1 = new Voiture();

            Console.WriteLine(Voiture.compteurVoiture);
            Console.WriteLine(v1.marque);
            Console.WriteLine(v1.Couleur);
            v1.marque = "Opel";
            Voiture v2 = new Voiture();
            Console.WriteLine(Voiture.compteurVoiture);
            v2.marque = "Fiat";
            Console.WriteLine(v1.marque);
            Console.WriteLine(v2.marque);
            Console.WriteLine(v2.Couleur);
            //
            Console.WriteLine(Voiture.ComparaisonPlaque(v1, v2));

            Console.WriteLine(v1.TestcompPlaque(v2));
            //
            Console.WriteLine(v1.Vitesse);
            v1.Accelerer(10);
            Console.WriteLine(v1.Vitesse);
            v1.Accelerer(20);
            Console.WriteLine(v1.Vitesse);
            v1.Freiner(5);
            Console.WriteLine(v1.Vitesse);
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();
            Console.WriteLine(v1.Vitesse);
            Console.WriteLine(v1.EstArreter());
            //
            Voiture v3 = new Voiture("Bmw", "fr-7585", "Noir");
            Console.WriteLine(Voiture.compteurVoiture);
            Console.WriteLine(v3.plaque);
            Console.WriteLine(v3.marque);
            Console.WriteLine(v3.Couleur);
            Console.WriteLine(v3.Vitesse);

            Console.WriteLine("Initialiseur");
            // Initialiseur d'objet => uniquement pour les attributs et les propriétés visible 
            // Voiture v4 = new Voiture("Seat", "fr-75455", "Orange") { vitesse=65};
            Voiture v4 = new Voiture("Seat", "fr-75455", "Orange");
            v4.Vitesse = 65;
            Console.WriteLine(Voiture.compteurVoiture);
            Console.WriteLine(v4.plaque);
            Console.WriteLine(v4.marque);
            Console.WriteLine(v4.Couleur);
            Console.WriteLine(v4.Vitesse);
            Voiture v5 = new Voiture { marque = "Honda", plaque = "fr-92045", Couleur = "Blanc" };
            Console.WriteLine(Voiture.compteurVoiture);
            Console.WriteLine(v5.plaque);
            Console.WriteLine(v5.marque);
            Console.WriteLine(v5.Couleur);
            Console.WriteLine(v5.Vitesse);

            //
            Voiture vp1 = new Voiture();
            Console.WriteLine(Voiture.compteurVoiture);
            Voiture vp2 = vp1;
            vp1 = null;
            vp2 = null;
            GC.Collect(); // appel explicite au garbage collector

            Proprietaire proprietaire = new Proprietaire("John", "Doe");
            v5.owner = proprietaire;

            // Exercice Compte Bancaire
            CompteBancaire cb = new CompteBancaire();
            cb.solde = 100;
            cb.titulaire = "John Doe";
            // cb.iban = "fr59-0000-0303";

            cb.Afficher();
            cb.Crediter(150.0);
            cb.Afficher();
            cb.Debiter(25.0);
            cb.Afficher();
            Console.WriteLine(cb.EstPositif());
            cb.Debiter(400.0);
            Console.WriteLine(cb.EstPositif());

            CompteBancaire cb2 = new CompteBancaire(230.0, "Antoine Bereto");
            cb2.Afficher();

            // Exercice Point
            Point p = new Point();
            p.x = 1.0;
            p.y = 2.0;
            p.Afficher();
            p.Deplacer(2, -1);
            p.Afficher();
            Console.WriteLine(p.Norme());
            Point p1 = new Point(5, 6);
            p1.Afficher();
            Console.WriteLine(Point.DistanceXY(p, p1));

            // Exercice Cercle
            Point pt1 = new Point(1, 1);
            Cercle cercle1 = new Cercle(pt1, 2);
            Point pt2 = new Point(2, 1);
            Cercle cercle2 = new Cercle(pt2, 4);
            Console.WriteLine(Cercle.Collision(cercle1, cercle2));
            Point pp = new Point(1, 2);
            Console.WriteLine(cercle1.Inclusion(pp));
            Point pp2 = new Point(10, 2);
            Console.WriteLine(cercle1.Inclusion(pp2));

            //Exemple CompteBancaireP
            Adresse adr1 = new Adresse("Rue esquermoise", "Lille", 59000);
            Personne john = new Personne("Doe", "John", "john.doe.dawan.fr", adr1);
            CompteBancaireP bp = new CompteBancaireP(100.0, john);
            bp.Afficher();
            Personne antoine = new Personne("Bereto", "Antoine", "ba@gmail.com", new Adresse("Rue du molinel", "Lille", 59000));
            CompteBancaireP bp1 = new CompteBancaireP(200.0, antoine);
            bp1.Afficher();

            // Exemple indexeur
            Banque bnp = new Banque(john);
            bnp["0"] = bp;
            bnp["1"] = bp1;
            bnp["0"].Crediter(500.0);
            bnp["0"].Afficher();
            Console.WriteLine(bnp["0"].Titulaire.Adresse.CodePostal);

            // Exercice Indexeur
            Point3d pt3 = new Point3d(1, 6, 7);
            pt3.Afficher();
            pt3.X = 10;
            pt3.Afficher();
            pt3[1] = 20;
            pt3.Afficher();
            pt3[3] = 10;

            // Classe partielle
            Form1 fo = new Form1();
            fo.Calcul();
            fo.Afficher();

            // Classe static
            // Tools t = new Tools(); // on ne peut pas instancier une classe static
            string str = "azerty";
            Console.WriteLine(Tools.IsNotnull(str));
            Console.WriteLine(Tools.IsSizeMin(str, 3));
            string str2 = null;
            Console.WriteLine(Tools.IsNotnull(str2));
            // Math math = new Math();  // on ne peut pas instancier une classe static
            Console.WriteLine(Math.Abs(12.3));

            // Classe imbriqué
            Container container = new Container();
            container.TestOutside();

            // Structure
            TestStructure tstr = new TestStructure(23);
            tstr.Afficher();
            tstr.ModifData(3);
            tstr.Afficher();
            TestStructure ts1;
            ts1.data = 34;
            ts1.Afficher();

            TestStructure? ts2 = null;

            if (!ts2.HasValue)
            {
                Console.WriteLine("est null");
            }
            Console.ReadKey();
        }
    }
}
