﻿using System;

namespace _06_Poo
{
    class Container// par défaut internal 
    {
        private static string varClasse = "testStatic";

        private string varInstance = "testInstance";

        public void TestOutside()
        {
            Inner n = new Inner();
            n.TestInsideStatic();
            n.TestInsideInstance();
            n.TestInsideInstance(this);
        }
        class Inner // par défaut private
        {
            public void TestInsideStatic()
            {
                Console.WriteLine(varClasse);
            }

            public void TestInsideInstance()
            {
                Container c = new Container();
                Console.WriteLine(c.varInstance + "sans paramètre");
            }

            public void TestInsideInstance(Container c)
            {
                Console.WriteLine(c.varInstance);
            }
        }

    }
}
