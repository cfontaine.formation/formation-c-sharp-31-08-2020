﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataBase
{
    public class TestDb
    {
        public static void TestBdd()
        {
            string strCnx = "Server=localhost;Port=3306;Database=formation;Uid=formation;Pwd=dawan;";
            using (MySqlConnection cnx = new MySqlConnection(strCnx))
            {
                cnx.Open();
                string reqSql = "SELECT id,nom,prenom,email,telephone FROM t_users";
                MySqlCommand cmd = new MySqlCommand(reqSql, cnx);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine("id={0} nom={1} prenom={2} email={3} téléphone={4}", reader.GetInt32("id"), reader.GetString("nom"), reader.GetString("prenom"), reader.GetString("email"), reader.GetString("telephone"));
                }
                reader.Close();
            }
            Console.ReadKey();
        }
    }
}
