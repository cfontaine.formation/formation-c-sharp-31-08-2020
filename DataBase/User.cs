﻿using System;

namespace DataBase
{
    public class User : DbObject
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }

        public User(string nom, string prenom, string email, string telephone)
        {
            Nom = nom;
            Prenom = prenom;
            Email = email;
            Telephone = telephone;
        }

        public string[] ToStringArray()
        {
            string[] tabStr = new string[4];
            tabStr[0] = Prenom;
            tabStr[1] = Nom;
            tabStr[2] = Email;
            tabStr[3] = Telephone;
            return tabStr;
        }

        public override string ToString()
        {
            return String.Format("User[Id={0}, Nom= {1}, Prenom={2}, Email={3}, Téléphone={4}]", Id, Nom, Prenom, Email, Telephone);
        }
    }

}
