﻿using System;

namespace DataBase
{
    // Classe de base des objets qui peuvent être gèré par GenericDao
    // Elle permet de gèrer l'Id pour les sous-classes
    public abstract class DbObject
    {
        public long Id { get; set; }

        public override bool Equals(object obj)
        {
            return obj is DbObject @object &&
                   Id == @object.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("DbObject[Id={0}]", Id);
        }
    }
}
