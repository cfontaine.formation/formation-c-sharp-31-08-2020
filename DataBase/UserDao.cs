﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace DataBase
{
    // Dao pour la classe User
    public class UserDao : GenericDao<User>
    {
        protected override void Create(User entity, MySqlConnection cnx)
        {
            string req = "INSERT INTO t_users(nom,prenom,email,telephone) VALUES(@nom,@prenom,@email,@telephone)";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@nom", entity.Nom);
            cmd.Parameters.AddWithValue("@prenom", entity.Prenom);
            cmd.Parameters.AddWithValue("@email", entity.Email);
            cmd.Parameters.AddWithValue("@telephone", entity.Telephone);
            cmd.ExecuteNonQuery();
            entity.Id = (int)cmd.LastInsertedId;
        }

        protected override void Delete(User entity, MySqlConnection cnx)
        {
            string req = "DELETE FROM t_users WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(req, cnx);
            cmd.Parameters.AddWithValue("@id", entity.Id);
            cmd.ExecuteNonQuery();
        }

        protected override User Read(int id, MySqlConnection cnx)
        {
            User user = null;
            string reqSql = "SELECT nom,prenom,email,telephone FROM t_users WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(reqSql, cnx);
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                user = new User(reader.GetString("nom"), reader.GetString("prenom"), reader.GetString("email"), reader.GetString("telephone"));
                user.Id = id;
            }
            reader.Close();
            return user;
        }

        protected override List<User> ReadALL(MySqlConnection cnx)
        {
            List<User> users = new List<User>();
            string reqSql = "SELECT id,nom,prenom,email,telephone FROM t_users";
            MySqlCommand cmd = new MySqlCommand(reqSql, cnx);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                User u = new User(reader.GetString("nom"), reader.GetString("prenom"), reader.GetString("email"), reader.GetString("telephone"));
                u.Id = reader.GetInt32("id");
                users.Add(u);
            }
            reader.Close();
            return users;
        }

        protected override void Update(User entity, MySqlConnection cnx)
        {
            string reqSql = "UPDATE t_users SET nom = @nom, prenom=@prenom, email=@email, telephone=@telephone WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(reqSql, cnx);
            cmd.Parameters.AddWithValue("@nom", entity.Nom);
            cmd.Parameters.AddWithValue("@prenom", entity.Prenom);
            cmd.Parameters.AddWithValue("@email", entity.Email);
            cmd.Parameters.AddWithValue("@telephone", entity.Telephone);
            cmd.Parameters.AddWithValue("@id", entity.Id);
            cmd.ExecuteNonQuery();
        }
    }

}
