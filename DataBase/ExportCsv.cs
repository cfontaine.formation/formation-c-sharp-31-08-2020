﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DataBase
{
    public static class ExportCsv
    {
        // Importer un fichier Csv et retourner une liste d'utilisateur
        public static List<User> Import(string path)
        {
            List<User> users = new List<User>();
            using (StreamReader sr = new StreamReader(path))
            {
                string str = "";
                while (!sr.EndOfStream)
                {
                    str = sr.ReadLine();
                    string[] tabStr = str.Split(';');
                    if (tabStr.Length == 4)
                    {
                        users.Add(new User(tabStr[0], tabStr[1], tabStr[2], tabStr[3]));
                    }
                    else
                    {
                        throw new FormatException("Le contenu du fichier le format");
                    }
                }
                return users;
            }
        }

        // Exporter une liste d'utilisateur dans un fichier Csv 
        public static void Export(List<User> users, string path)
        {
            using (StreamWriter sw = new StreamWriter(path, false))
            {
                foreach (User user in users)
                {
                    sw.WriteLine(string.Format("{0};{1};{2};{3}", user.Prenom, user.Nom, user.Email, user.Telephone));
                }
            }
        }

    }
}
