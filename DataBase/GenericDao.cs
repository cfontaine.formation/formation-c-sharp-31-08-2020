﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataBase
{
    // Classe de base pour les dao
    public abstract class GenericDao<T> where T : DbObject
    {
        protected static string cnxStr;
        public static string ConnectionChaine { set=> cnxStr=value; }
        public void SaveOrUpdate(T entity)
        {
            MySqlConnection cnx = GetConnection();
            if (entity.Id == 0)
            {
                Create(entity, cnx);
            }
            else
            {
                Update(entity, cnx);
            }
            cnx.Close();
        }

        public List<T> ReadAll()
        {
            MySqlConnection cnx = GetConnection();
            List<T> lst = ReadALL(cnx);
            cnx.Close();
            return lst;
        }

        public T Read(int id)
        {
            MySqlConnection cnx = GetConnection();
            T res = Read(id, cnx);
            cnx.Close();
            return res;
        }

        public void Delete(T entity)
        {
            MySqlConnection cnx = GetConnection();
            Delete(entity, cnx);
            cnx.Close();
        }

        protected MySqlConnection GetConnection()
        {
            MySqlConnection cnx = new MySqlConnection(cnxStr);
            cnx.Open();
            return cnx;
        }

        protected abstract void Create(T entity, MySqlConnection cnx);
        protected abstract List<T> ReadALL(MySqlConnection cnx);
        protected abstract T Read(int id, MySqlConnection cnx);
        protected abstract void Update(T entity, MySqlConnection cnx);
        protected abstract void Delete(T entity, MySqlConnection cnx);
    }

}
