﻿namespace _08_Polymorphisme
{
    abstract class Personnage
    {
        public string Nom;
        public int PtVie { get; set; } = 20;
        public int PtAttq { get; set; } = 15;
        public int PtDef { get; set; } = 10;


        public Personnage(string nom, int ptVie, int ptAttq, int ptDef)
        {
            Nom = nom;
            PtVie = ptVie;
            PtAttq = ptAttq;
            PtDef = ptDef;
        }

        public abstract void Attaquer(Personnage ennemi);


        public bool EstMort()
        {
            return PtVie <= 0;
        }


        public override string ToString()
        {
            return string.Format("Personnage[nom={0}, ptVie = {1}, ptAttq = {2}, ptDef ={3}]", Nom, PtVie, PtAttq, PtDef);
        }
    }

}
