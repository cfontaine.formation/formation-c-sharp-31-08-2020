﻿namespace _08_Polymorphisme
{
    class Hero : Personnage
    {
        public Arme Arme { get; set; }


        public Hero(string nom, int ptVie, int ptAttq, int ptDef, Arme arme = null) : base(nom, ptVie, ptAttq, ptDef)
        {
            Arme = arme;
        }

        public override void Attaquer(Personnage ennemi)
        {
            ennemi.PtVie -= (Arme == null ? PtAttq : Arme.PtDestruction) - ennemi.PtDef;
        }

        public override string ToString()
        {
            return string.Format("Hero[nom={0}, ptVie = {1}, ptAttq = {2}, ptDef ={3} {4}]", Nom, PtVie, PtAttq, PtDef, Arme != null ? Arme.ToString() : " Sans Arme");
        }
    }


}
