﻿using _06_Poo;
using System;

namespace _08_Polymorphisme
{
    class Program
    {
        static void Main(string[] args)
        {
            //Animal a = new Animal(); // Impossible la classe est abstraite
            //a.EmettreUnSon();

            Chien chien1 = new Chien("Laica");
            chien1.EmettreUnSon();
            Animal ac = new Chien("Snoopy"); // On peut créer une instance de Chien (classe fille) qui sera référencée par une référence Animal (classe mère)
                                             // L'objet chien sera vue comme un Animal on ne pourra pas accèder au propriété et au méthode  propre au chien Nom,...

            ac.EmettreUnSon();               // Comme la méthode est virtual dans Animal et est rédéfinie dans Chien, c'est la méthode de Chien qui sera appelée
            if (ac is Chien)                 // test si ac est de "type" Chien
            {
                // Chien chien2 = (Chien)ac;    // Pour passer d'une super-classe à une sous-classe, il faut le faire expicitement avec un cast ou avec l'opérateur as
                Chien chien2 = ac as Chien;     // as equivalant à un cast pour un objet
                Console.WriteLine(chien2.Nom);  // avec la référence chien2 on a bien accès à toutes les méthodes de la classe chien
            }

            Animal ac2 = new Chat("Tom");
            ac2.EmettreUnSon();

            Animal[] tab = new Animal[5];
            tab[0] = new Chien("aaaa");
            tab[1] = new Chien("bbbb");
            tab[2] = new Chat("ccccc");
            tab[3] = new Canard();
            tab[4] = new Chat("eeeee");

            foreach (Animal animal in tab)
            {
                animal.EmettreUnSon();
            }

            ICanFly icf = new Canard();       // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            icf.Decoller();                   // Le canard n'est vu que comme un interface ICanFly, on ne peut utiliser que les méthode de ICanFly

            ICanWalk[] tabIcw = new ICanWalk[5];
            tabIcw[0] = new Chien("aaaa");
            tabIcw[1] = new Chien("bbbb");
            tabIcw[2] = new Chat("ccccc");
            tabIcw[3] = new Canard();
            tabIcw[4] = new Chat("eeeee");

            foreach (ICanWalk can in tabIcw)
            {
                can.Marcher();
            }

            // Exercice Polymorphisme
            Hero arthur = new Hero("arthur", 50, 12, 8);
            Console.WriteLine(arthur);
            Hero gobelin = new Hero("Gobelin", 100, 15, 10);
            Arme excalibur = new Arme("excalibur", 35);
            arthur.Arme = excalibur;
            Combattre(arthur, gobelin);

            Mage merlin = new Mage("Merlin", 100, 3, 10, 4, 45);
            merlin.UseMagie = true;
            Hero orc = new Hero("Orc", 100, 15, 10);
            Combattre(merlin, orc);

            // Surcharge opérateur
            Point p1 = new Point(1, 6);
            Point p2 = new Point(-4, 2);
            Point r = p1 + p2;
            r += p2;
            r *= 3.0;

            Console.WriteLine(r);
            Point r2 = -p1;
            Console.WriteLine(r2);

            Console.WriteLine(p1 == p2); // opérateur redéfinie => on compare les objets
            Console.WriteLine(p1 != p2);
            Console.ReadKey();
        }

        static void Combattre(Personnage p1, Personnage p2)
        {
            while (!p1.EstMort() && !p2.EstMort())
            {
                p1.Attaquer(p2);
                p2.Attaquer(p1);
                Console.WriteLine("________________________");
                Console.WriteLine("{0}\n{1}", p1, p2);
            }
            Console.WriteLine("===>  Vainqueur  {0} <===", p1.EstMort() ? p2 : p1);

        }

    }
}
