﻿using System;

namespace _08_Polymorphisme
{
    class Chat : Animal, ICanWalk
    {

        public int NbVie { get; set; } = 9;
        public string Nom { get; set; }

        public Chat(string nom)
        {
            Nom = nom;
        }

        public Chat(string nom, double poid) : base(poid, 4)
        {
            Nom = nom;
        }

        public override void EmettreUnSon()
        {
            Console.WriteLine("Le chat miaule");
        }

        public void Marcher()
        {
            Console.WriteLine("Le chat marche");
        }

        public void Courrir()
        {
            Console.WriteLine("Le chat court");
        }
    }
}
