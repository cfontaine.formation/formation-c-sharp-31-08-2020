﻿using System;

namespace _06_Poo
{
    class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point()
        {
        }

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public void Deplacer(double tx, double ty)
        {
            X += tx;
            Y += ty;
        }

        public double Norme()
        {
            return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
        }

        public static double Distance(Point pa, Point pb)
        {
            return Math.Sqrt(Math.Pow((pb.X - pa.X), 2) + Math.Pow((pb.Y - pa.Y), 2));
        }

        // Surcharge d'opérateur binaire
        public static Point operator +(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static Point operator *(Point p1, double m)
        {
            return new Point(p1.X * m, p1.Y * m);
        }

        // Surcharge d'opérateur unaire
        public static Point operator -(Point p)
        {
            return new Point(-p.X, -p.Y);
        }

        // Surcharge de comparaison
        public static bool operator ==(Point p1, Point p2)
        {
            return p1.X == p2.X && p1.Y == p2.Y;
        }

        public static bool operator !=(Point p1, Point p2)
        {
            //return p1.X != p2.X || p1.Y != p2.Y;
            // ou
            return !(p1 == p2);
        }

        public override bool Equals(object obj)
        {
            return obj is Point point &&
                   X == point.X &&
                   Y == point.Y;
        }

        public override int GetHashCode()
        {
            int hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return string.Format("({0},{1})", X, Y);
        }

    }
}
