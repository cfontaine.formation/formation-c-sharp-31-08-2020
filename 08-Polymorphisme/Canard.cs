﻿using System;

namespace _08_Polymorphisme
{
    class Canard : Animal, ICanWalk, ICanFly, IComparable    // On peut implémenter plusieurs d'interface
    {

        public override void EmettreUnSon()
        {
            Console.WriteLine("Coin Coin");
        }

        public void Marcher()
        {
            Console.WriteLine("Le canard Marche");
        }

        public void Courrir()
        {
            Console.WriteLine("Le canard Court");
        }

        public void Decoller()
        {
            Console.WriteLine("Le canard décolle");
        }

        public void Atterrir()
        {
            Console.WriteLine("Le canard atterri");
        }

        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
