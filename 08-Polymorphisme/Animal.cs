﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_Polymorphisme
{
   abstract class Animal    // Animal est une classe abstraite, elle ne peut pas être instantiée elle même 
    {                       // mais uniquement par l'intermédiaire de ses classses filles

        public double Poid { get; set; }

        public int NombrePatte { get; set; } = 2;

        public Animal()
        {
        }

        public Animal(double poid, int nombrePatte)
        {
            Poid = poid;
            NombrePatte = nombrePatte;
        }

        public abstract void EmettreUnSon();    // Méthode abstraite qui doit obligatoirement redéfinit par les classes filles
        //{
        //    Console.WriteLine("L'animal fait un son");
        //}

        public override string ToString()
        {
            return String.Format("Animal [Poid={0} Nombre de patte={1}]", Poid, NombrePatte);
        }
    }
}
