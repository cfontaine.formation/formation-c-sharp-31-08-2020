﻿using System;

namespace _08_Polymorphisme
{
    class Chien : Animal, ICanWalk // La classe Chien hérite de la classe Animal et implémente l'interface ICanWalk
    {
        public string Nom { get; set; }

        public Chien(string nom) : base(2, 4)
        {
            Nom = nom;
        }

        public Chien(string nom, double poid) : base(poid, 4)
        {
            Nom = nom;
        }

        public override void EmettreUnSon()         // Redéfinition obligatoire de la méthode abstraite EmmettreUnSon de la classe Animal 
        {
            Console.WriteLine("Le chien aboie");
        }

        public void Marcher()                       // Impléméntation de la méthode Marcher de l'interface ICanWalk
        {
            Console.WriteLine("Le chien marche");
        }

        public void Courrir()
        {
            Console.WriteLine("Le chien court");    // Impléméntation de la méthode Courrir de l'interface ICanWalk
        }
    }
}
