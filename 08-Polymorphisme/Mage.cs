﻿namespace _08_Polymorphisme
{
    class Mage : Personnage
    {
        public int PtMagie { get; set; } = 10;
        public int PtAttaqMagie { get; set; } = 20;
        public bool UseMagie { get; set; }
        public Mage(string nom, int ptVie, int ptAttaq, int ptDef, int ptMagie, int ptAttaqMagie) : base(nom, ptVie, ptAttaq, ptDef)
        {
            PtMagie = ptMagie;
            PtAttaqMagie = ptAttaqMagie;
        }
        public override void Attaquer(Personnage ennemi)
        {
            if (UseMagie && PtMagie > 0)
            {
                ennemi.PtVie -= PtAttaqMagie - ennemi.PtDef;
                PtMagie--;
            }
            else
            {
                ennemi.PtVie -= PtAttq - ennemi.PtDef;
            }
        }

        public override string ToString()
        {
            return string.Format("Mage[nom={0}, ptVie = {1}, ptAttq = {2}, ptDef ={3} ptMagie ={4} ptAttaqMAgie={5}]", Nom, PtVie, PtAttq, PtDef, PtMagie, PtAttaqMagie);
        }
    }
}
