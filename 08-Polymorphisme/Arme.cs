﻿namespace _08_Polymorphisme
{
    class Arme
    {
        public string Nom { get; set; }

        public int PtDestruction { get; set; } = 10;

        public Arme(string nom, int ptAttq)
        {
            Nom = nom;
            PtDestruction = ptAttq;
        }
        public override string ToString()
        {
            return string.Format("Arme [nom= {0}, ptAttq={1}]", Nom, PtDestruction);
        }
    }

}
