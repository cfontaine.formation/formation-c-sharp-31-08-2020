﻿namespace _08_Polymorphisme
{
    interface ICanFly
    {
        void Decoller();

        void Atterrir();
    }
}
