﻿using System;

namespace _07_heritage
{
    class VoiturePrioritaire : Voiture
    {

        public bool Gyro { get; set; }

        public VoiturePrioritaire() // hériter implicitement du constructeur par défaut de la classe mère
        {
            Console.WriteLine("Constructeur classe Fille");
        }

        public VoiturePrioritaire(string marque, string plaque, string couleur, bool gyro) : base(marque, plaque, couleur)
        {
            Gyro = gyro;
        }

        public void AlummerGyro()
        {
            Gyro = true;
        }

        public void EteindreGyro()
        {
            Gyro = false;
        }

        public new void Accelerer(int vitesseAcc)
        {
            Console.WriteLine("Accelerer priotaire");
            // Vitesse += vitesseAcc * 2;
            base.Accelerer(vitesseAcc * 2); // base pour appeler une méthode de la classe mère
        }

        public override string ToString()
        {
            return string.Format("Voiture Prioritaire [ gyro={0} {1}]", Gyro, base.ToString());
        }
    }
}
