﻿//using Other;
using System;
using Other.Test;

//using Consoleo = Other.Console;

namespace _07_heritage
{
    class Program
    {
        static void Main(string[] args)
        {
            VoiturePrioritaire vp = new VoiturePrioritaire();
            // vp.Accelerer(10);
            // Console.WriteLine(vp.Vitesse);
            vp.AlummerGyro();
            System.Console.WriteLine(vp.Gyro);
            Console.WriteLine(VoiturePrioritaire.compteurVoiture);

            // Redéfinition
            vp.Accelerer(10);
            Console.WriteLine(vp.Vitesse);

            // Object
            Voiture v = new Voiture("Opel", "Fr-2567", "Orange");
            Voiture vr = v;
            Console.WriteLine(v);
            Console.WriteLine(vp);
            Voiture vprime = new Voiture("Opel", "Fr-2567", "Orange");

            Console.WriteLine(v == vprime); // compare l'égalité entre les références
            Console.WriteLine(v == vr); // compare l'égalité entre les références

            Console.WriteLine(v.Equals(vprime)); // compare l'égalité entre les objets avec la mèthode Equals redéfinie
            Console.WriteLine(v.Equals(vr)); // compare l'égalité entre les objets avec la mèthode Equals redéfinie 

            //  CompteEpargne ce = new CompteEpargne(6, "John Doe");
            //  ce.Afficher();
            //  ce.CalculInterets();
            //  ce.Afficher();
            //  TestEspaceDeNom t = new TestEspaceDeNom();
            ////  Consoleo co = new Consoleo();
            ////  co.Afficher();

            // Shadowing
            VoiturePrioritaire v4 = new VoiturePrioritaire("fiat", "fr59000", "Vert", true);
            v4.Accelerer(10);
            Console.WriteLine(v4);
            Console.ReadKey();
        }
    }
}
