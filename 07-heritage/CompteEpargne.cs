﻿namespace _07_heritage
{
    class CompteEpargne : CompteBancaire
    {

        public CompteEpargne(double taux)// : base()
        {
            Taux = taux;
        }
        public CompteEpargne(double taux, string titulaire) : base(titulaire)
        {
            Taux = taux;
        }

        public double Taux { get; set; }

        public void CalculInterets()
        {
            solde *= (1 + Taux / 100.0);
        }
    }
}
