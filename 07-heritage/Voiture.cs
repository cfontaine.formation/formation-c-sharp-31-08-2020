﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_heritage
{
    /*sealed*/
    class Voiture //  sealed empecher l'héritage de cette classe
    {
        // Variables d'instances => Etat
        public string marque = "Ford";
        public string plaque;
        private int vitesse;
        // private string couleur="Rouge";

        // Variable de classe
        public static int compteurVoiture;

        // Constructeurs
        public Voiture()
        {
            Console.WriteLine("Constructeur classe Mère");
            compteurVoiture++;
        }

        public Voiture(string marque, string plaque) : this()
        {
            this.marque = marque;
            this.plaque = plaque;
            //    compteurVoiture++;
        }

        public Voiture(string marque, string plaque, string couleur) : this(marque, plaque) // Chainnage de constructeur
        {
            //this.marque = marque; // inutile fait par le constructeur 2 paramètres
            //this.plaque = plaque; //
            Couleur = couleur;
            //   compteurVoiture++;
        }

        // Constructeur Statique
        static Voiture()
        {
            Console.WriteLine("Execution du constructeur statique");
        }

        // Destructeur
        ~Voiture()
        {
            Console.WriteLine("Destructeur");
        }

        // Méthodes d'instances => comportement
        public void Accelerer(int vitesseAcc)
        {
            Console.WriteLine("Accelerer voiture");
            if (vitesseAcc > 0)
            {
                vitesse += vitesseAcc;
            }
        }

        public void Freiner(int vitesseFre)
        {
            if (vitesseFre > 0)
            {
                vitesse -= vitesseFre;
            }
            if (vitesse < 0)
            {
                vitesse = 0;
            }
        }

        public void Arreter()
        {

            vitesse = 0;
        }

        public bool EstArreter()
        {
            return vitesse == 0;
        }

        public bool TestcompPlaque(Voiture v)
        {
            return ComparaisonPlaque(this, v);
        }

        public static void TestMethodeClasse()
        {
            //vitesse = 0; // On ne peut accèder à une variable d'instance dans une méthode de classe
            Console.WriteLine("Méthode de classe");
        }

        public static bool ComparaisonPlaque(Voiture va, Voiture vb)
        {
            return va.plaque == vb.plaque;
        }

        //public int GetVitesse()
        //{
        //    return vitesse;
        //}

        //public void SetVitesse(int vitesse)
        //{
        //    if (vitesse >= 0)
        //    {
        //        this.vitesse = vitesse;
        //    }
        //    else
        //    {
        //        throw new Exception("Erreur vitesse négative");
        //    }
        //}

        public int Vitesse
        {
            get
            {
                return vitesse;
            }
            set
            {
                if (value >= 0)
                {
                    vitesse = value;
                }
            }
        }

        //public string Couleur
        //{
        //    get
        //    {
        //        return couleur;
        //    }
        //    set
        //    {
        //        couleur = value;
        //    }
        //}
        public string Couleur { get; set; }

        public override string ToString()
        {
            return string.Format("Voiture [ marque={0}, couleur={1}, vitesse={2}, plaque={3}]", marque, Couleur, vitesse, plaque);
        }

        public override bool Equals(object obj)
        {
            return obj is Voiture voiture &&
                   marque == voiture.marque &&
                   plaque == voiture.plaque &&
                   Couleur == voiture.Couleur;
        }

        public override int GetHashCode()
        {
            int hashCode = -1409694165;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(marque);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(plaque);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Couleur);
            return hashCode;
        }
    }
}
