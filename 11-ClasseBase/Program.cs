﻿using _12___Fichier_bibliotheque;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace _11_ClasseBase
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "Hello world";
            string str2 = "Bonjour";
            string str3 = "Bonjour";
            Console.WriteLine(str.Length);  // Length ->Nombre de caractère de la chaine de caractère

            // Comparaison 0-> égale, 1-> se trouve après, -1 -> se trouve avant dans l'ordre alphabètique
            // Il existe 2 méthodes: une d'instance et de classe
            Console.WriteLine(str.CompareTo(str2));
            Console.WriteLine(string.Compare(str2, str));
            Console.WriteLine(str2.Equals(str3));

            // Comparaison
            // On peut tester l'égalité de 2 chaines de caractères avec Equals et l'opérateur ==
            if ("azerty".Equals(str))
            {
                Console.WriteLine("chaine égale");
            }
            Console.WriteLine(str2 == str3);


            // Concaténation 
            // avec l'opérateur + ou avec la méthode de classe Concat
            Console.WriteLine(string.Concat(str, "aaa", "111"));
            Console.WriteLine(str + " azerty");
            // La méthode Join concatène les chaines  en les séparants par un caractère de séparation
            Console.WriteLine(string.Join(",", str, str2, "azerty"));

            // Découpe la chaine en un tableau de sous-chaine suivant un ou plusieurs séparateur, par défaut le caractère espace
            string str4 = "azerty,qsdfg,wxcv";
            string[] tabStr = str4.Split(',');
            foreach (string s in tabStr)
            {
                Console.WriteLine(s);
            }

            // Substring permet d'extraire une sous-chaine d'une chaine
            // de l'indice passé en paramètre juqu'à la fin de la chaine ou pour le nombre de caractère passé en paramètre
            Console.WriteLine(str.Substring(6));
            Console.WriteLine(str.Substring(6, 2));

            // Insère la chaine à partir de l'indice passé en paramètre
            Console.WriteLine(str.Insert(5, "----"));
            // Les caractères sont supprimés à partir de l'indice pour le nombre de caractère passé en paramètre
            Console.WriteLine(str.Remove(5, 1));

            // StartsWith retourne true si la chaine commence par la chaine passé en paramètre
            Console.WriteLine(str.StartsWith("Hello"));
            Console.WriteLine(str.StartsWith("Bonjour"));

            // IndexOf retourne la première occurence du caractère ou de la chaine passée en paramètre
            Console.WriteLine(str.IndexOf('o'));
            Console.WriteLine(str.IndexOf('o', 5));  // idem mais à partir de l'indice passé en paramètre
            Console.WriteLine(str.IndexOf('o', 8));

            // Remplace toutes les chaines (ou caratère) oldValue par newValue 
            Console.WriteLine(str.Replace('o', 'a'));

            // Retourne True si la chaine passée en paramètre est contenu dans la chaine
            Console.WriteLine(str.Contains("wo"));
            Console.WriteLine(str.Contains("jour"));

            // Aligne les caractères à droite en les complétant par un caractère à gauche pour une longueur spécifiée
            Console.WriteLine(str.PadLeft(50, '_'));
            // Aligne les caractères à gauche en les complétant par un caractère à droite pour une longueur spécifiée
            Console.WriteLine(str.PadRight(50, '_'));
            // Trim supprime les caractères de blanc du début et de la fin de la chaine
            Console.WriteLine("\t \n        azertyubvkbkd hgjsvsbvksb \t\t\t\n\n   ".Trim());
            Console.WriteLine("\t \n        azertyubvkbkd hgjsvsbvksb \t\t\t\n\n   ".TrimStart()); // idem uniquement en début de chaine
            Console.WriteLine("\t \n        azertyubvkbkd hgjsvsbvksb \t\t\t\n\n   ".TrimEnd()); // idem uniquement en fin de chaine

            // ToUpper convertie tous les caractères en majuscule
            Console.WriteLine(str.ToUpper());

            // On peut chainer l'appel des différentes méthodes
            Console.WriteLine("\t \n        azertyubvkbkd hgjsvsbvksb \t\t\t\n\n   ".Trim().ToUpper().Substring(10));

            StringBuilder sb = new StringBuilder("Hello");
            sb.Append("World");
            sb.Insert(5, "------------");
            string str5 = sb.ToString();
            Console.WriteLine(str5);

            // Exercice Chaine de caractères
            Console.Write("Saisir une chaine de caractère ");
            str = Console.ReadLine();
            Console.WriteLine(Inverser(str));
            Console.WriteLine(Palindrome("Radar"));
            Console.WriteLine(Palindrome("Bonjour"));
            Console.WriteLine(Acronyme("Organisation du traité de l’Atlantique Nord"));

            DateTime d = DateTime.Now; // DateTime.Now => récupérer l'heure et la date courante
            DateTime noel2012 = new DateTime(2012, 12, 25);
            Console.WriteLine(d);
            Console.WriteLine(noel2012.ToShortDateString());
            Console.WriteLine(noel2012.ToLongDateString());
            Console.WriteLine(noel2012.ToShortTimeString());
            Console.WriteLine(noel2012.ToLongTimeString());
            Console.WriteLine(d - noel2012);

            TimeSpan tsp = new TimeSpan(1, 3, 10, 0);   // TimeSpan => représente une durée
            Console.WriteLine(d.Add(tsp));

            Console.WriteLine(d.Hour);
            Console.WriteLine(d.Year);
            Console.WriteLine(d.Ticks);

            Console.WriteLine(DateTime.Parse("1932/01/10"));
            Console.WriteLine(d.ToString("dd-MM-yy"));
            Console.WriteLine(d > noel2012);

            // Collection faiblement typé => elle peut contenir tous types d'objets 
            ArrayList lst = new ArrayList();    // ArrayList = tableau à taille variable
            lst.Add("Azerty");
            lst.Add(1);
            lst.Add(true);

            Console.WriteLine(lst.Count);
            Console.WriteLine(lst[0]);
            foreach (object obj in lst) // parcours d'une collection
            {
                if (obj is string)   // is permet de tester le type de l'objet
                {
                    string str9 = obj as string;    // as equivaut à un cast pour les objets
                    Console.WriteLine(str9);
                }
            }
            // Collection fortement typée
            List<string> lst2 = new List<string>();
            lst2.Add("azerty");
            //lst2.Add(true);       // Erreur la liste ne peut contenir que des booléens
            lst2.Add("fgg");
            lst2.Add("bebrnr");
            lst2.Add("zfzrz");
            foreach (string stt in lst2)
            {
                Console.WriteLine(stt);
            }

            // Type généric
            Calcul<int> calcI = new Calcul<int>(12, 34);
            calcI.Afficher();
            Calcul<double> calcD = new Calcul<double>(12, 34);
            calcD.Afficher();
            //calcD.Test<string>("azerty");
            calcD.Test("azerty");
            calcD.Test(10);

            // Dictionary => association clé/valeur
            Dictionary<int, string> m = new Dictionary<int, string>();
            m.Add(12, "Bonjour");
            Console.WriteLine(m[12]);
            m.Add(1000, "Hello World");// Add => ajout d'un valeur associé à une clé
            m.Add(3, "asupprimer");
            m.Add(45, "amodifier");
            //m.Add(45, "autre");       // Erreur on ne peut pas ajouter une valeur deux fois pour une même clé
            m[45] = "nouvelle Valeur";    // Pour modifer, la valeur qui a pour clé 45
            m.Remove(3);                // Supprimer une clé/valeur de la collection 

            foreach (KeyValuePair<int, string> valueP in m)  // parcourir un dictionnary
            {
                Console.WriteLine("Key={0}  Value={1}", valueP.Key, valueP.Value);
            }

            // Parcourrir un collection avec un itérateur
            IEnumerator en = lst2.GetEnumerator();
            en.Reset();
            while (en.MoveNext())
            {
                Console.WriteLine(en.Current);
            }


            foreach (var v in genNumber40())
            {
                Console.WriteLine(v);
            }

            // I/O
            TestFichier.TestDll();  // Test d'appel d'une méthode qui est dans le projet ExportLibrary
            TestFichier.InfoLecteur();
            TestFichier.Repertoire(@"C:\Formations\TestIO");
            // Fichier texte
            TestFichier.EcrireFichierText(@"C:\Formations\TestIO\Text1.txt");
            TestFichier.LireFichierTexte(@"C:\Formations\TestIO\Text1.txt");
            // Fichier binaire
            TestFichier.EcrireFichierBinaire(@"c:\Formations\TestIO\data.bin");
            TestFichier.LireFichierBinaire(@"c:\Formations\TestIO\data.bin");
            // Exercice parcours d'un système de fichiers
            TestFichier.ScanRep(@"C:\Formations\Tools\apache-tomcat-9.0.31");
            Console.ReadKey();
        }

        // Écrire la méthode Inverser qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
        // exemple : bonjour => ruojnob
        static string Inverser(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                sb.Append(str[str.Length - i - 1]);
            }
            return sb.ToString();
        }

        // Écrire une méthode qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est un palindrome
        // Ex: SOS, radar
        static bool Palindrome(string str)
        {
            string tmp = str.Trim().ToLower();
            return tmp == Inverser(tmp);
        }

        //Faire une méthode de classe qui prend en paramètre une phrase et qui retourne un acronyme
        //Les mots de la phrase sont pris en compte s'ils contiennent plus de 2 lettres
        //Ex:   Comité international olympique → CIO
        //      Organisation du traité de l’Atlantique Nord → OTAN
        static String Acronyme(string str)
        {
            string acr = "";
            string[] tabStr = str.Trim().ToUpper().Replace('’', ' ').Split();
            foreach (string s in tabStr)
            {
                if (s.Length > 2)
                {
                    acr += s[0];
                }
            }
            return acr;
        }

        static IEnumerable<int> genNumber40()
        {
            int i = 2;
            do
            {
                yield return i;
            } while (i++ < 40);
        }

    }
}
