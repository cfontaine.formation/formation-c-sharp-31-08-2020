﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_ClasseBase
{
     class Calcul<T>
    {
        public T A { get; set; }
        public T B { get; set; }

        public Calcul(T a, T b)
        {
            A = a;
            B = b;
        }

        public void Afficher()
        {
            Console.WriteLine(A + "  " + B);
        } 
        public  void Test<U> (U val)
        {
            Console.WriteLine(val);
        }
       
    }
}
