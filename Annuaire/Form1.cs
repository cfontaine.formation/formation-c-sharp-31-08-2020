﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using DataBase;

namespace Annuaire
{
    public partial class Form1 : Form
    {
        private UserDao userDao = new UserDao();
        public Form1()
        {
            InitializeComponent();
            // Lecture de la chaine de connexion qui est stockée dans le fichier App.Config
            // Il faut ajouter la référence -> assembly -> system.configuration
            UserDao.ConnectionChaine = ConfigurationManager.ConnectionStrings["chCnxMysql"].ConnectionString;
            LoadUserViewList();
        }

        private void BtnAjouter_Click(object sender, EventArgs e)
        {
            DialogResult dg = MessageBox.Show("Voulez vous ajouter l'utilisateur", "Ajouter un utilisateur", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dg.HasFlag(DialogResult.Yes))
            {
                User u = new User(textBoxNom.Text, textBoxPrenom.Text, textBoxEmail.Text, TextBoxTelphone.Text);
                ListViewItem item = new ListViewItem(u.ToStringArray());
                listUser.Items.Add(item);
                userDao.SaveOrUpdate(u);
            }
        }


        private void BtnImporter_Click(object sender, EventArgs e)
        {
            string path = ImportDialogBox();
            if (path != string.Empty)
            {
                try {

                    List<User> users = ExportCsv.Import(path);
                    foreach (User u in users) {
                        userDao.SaveOrUpdate(u);
                    }
                    LoadUserViewList();
                    MessageBox.Show(string .Format("{0} utilisateur(s) importer",users.Count), "Importation utilisateur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (FormatException) 
                {
                    MessageBox.Show("Le fichier n'est pas au bon format", "Erreur format", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }


        private void BtnExporter_Click(object sender, EventArgs e)
        {
            string path = ExportDialogBox();
            if (path != string.Empty)
            {
                List<User> users = userDao.ReadAll();
                ExportCsv.Export(users, path);
            }
        }

        private void BtnInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Formation C# Dawan septembre 2020", "Information", MessageBoxButtons.OK,MessageBoxIcon.Information);
        }


        private string ImportDialogBox()
        {
            string path = string.Empty;
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Title = "Importer des utilisateurs";
                openFileDialog.Filter = "Fichier (.csv)| *.csv" ;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    path = openFileDialog.FileName;
                }
            }
            return path;
        }

        private string ExportDialogBox()
        {
            string path = string.Empty;
            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Title = "Exorter des utilisateurs";
                saveFileDialog.Filter = "Fichier (.csv)| *.csv";
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    path = saveFileDialog.FileName;
                }
            }
            return path;
        }

        private void LoadUserViewList()
        {
            List<User> users=userDao.ReadAll();
            listUser.Items.Clear();
            foreach (User u in users)
            {
                ListViewItem item = new ListViewItem(u.ToStringArray());
                listUser.Items.Add(item);
            }

        }

    }
}
