﻿namespace Annuaire
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnInfo = new System.Windows.Forms.Button();
            this.textBoxPrenom = new System.Windows.Forms.TextBox();
            this.textBoxNom = new System.Windows.Forms.TextBox();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.TextBoxTelphone = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.listUser = new System.Windows.Forms.ListView();
            this.columnPrenon = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columNom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTelephone = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.BtnImporter = new System.Windows.Forms.Button();
            this.BtnExporter = new System.Windows.Forms.Button();
            this.BtnAjouter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnInfo
            // 
            this.BtnInfo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnInfo.Location = new System.Drawing.Point(697, 327);
            this.BtnInfo.Name = "BtnInfo";
            this.BtnInfo.Size = new System.Drawing.Size(54, 44);
            this.BtnInfo.TabIndex = 0;
            this.BtnInfo.Text = "Info";
            this.BtnInfo.UseVisualStyleBackColor = true;
            this.BtnInfo.Click += new System.EventHandler(this.BtnInfo_Click);
            // 
            // textBoxPrenom
            // 
            this.textBoxPrenom.Location = new System.Drawing.Point(47, 68);
            this.textBoxPrenom.Name = "textBoxPrenom";
            this.textBoxPrenom.Size = new System.Drawing.Size(215, 22);
            this.textBoxPrenom.TabIndex = 5;
            // 
            // textBoxNom
            // 
            this.textBoxNom.Location = new System.Drawing.Point(47, 126);
            this.textBoxNom.Name = "textBoxNom";
            this.textBoxNom.Size = new System.Drawing.Size(215, 22);
            this.textBoxNom.TabIndex = 6;
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Location = new System.Drawing.Point(47, 190);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(215, 22);
            this.textBoxEmail.TabIndex = 7;
            // 
            // TextBoxTelphone
            // 
            this.TextBoxTelphone.Location = new System.Drawing.Point(47, 251);
            this.TextBoxTelphone.Name = "TextBoxTelphone";
            this.TextBoxTelphone.Size = new System.Drawing.Size(215, 22);
            this.TextBoxTelphone.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Prénom";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Nom";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Email";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 231);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "Téléphone";
            // 
            // listUser
            // 
            this.listUser.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnPrenon,
            this.columNom,
            this.columnEmail,
            this.columnTelephone});
            this.listUser.Location = new System.Drawing.Point(305, 38);
            this.listUser.Name = "listUser";
            this.listUser.Size = new System.Drawing.Size(469, 235);
            this.listUser.TabIndex = 13;
            this.listUser.UseCompatibleStateImageBehavior = false;
            this.listUser.View = System.Windows.Forms.View.Details;
            // 
            // columnPrenon
            // 
            this.columnPrenon.Text = "Prénom";
            // 
            // columNom
            // 
            this.columNom.Text = "Nom";
            // 
            // columnEmail
            // 
            this.columnEmail.Text = "Email";
            // 
            // columnTelephone
            // 
            this.columnTelephone.Text = "Téléphone";
            // 
            // BtnImporter
            // 
            this.BtnImporter.Location = new System.Drawing.Point(325, 327);
            this.BtnImporter.Name = "BtnImporter";
            this.BtnImporter.Size = new System.Drawing.Size(150, 44);
            this.BtnImporter.TabIndex = 14;
            this.BtnImporter.Text = "Importer (.csv)";
            this.BtnImporter.UseVisualStyleBackColor = true;
            this.BtnImporter.Click += new System.EventHandler(this.BtnImporter_Click);
            // 
            // BtnExporter
            // 
            this.BtnExporter.Location = new System.Drawing.Point(498, 327);
            this.BtnExporter.Name = "BtnExporter";
            this.BtnExporter.Size = new System.Drawing.Size(152, 44);
            this.BtnExporter.TabIndex = 15;
            this.BtnExporter.Text = "Exporter (.csv)";
            this.BtnExporter.UseVisualStyleBackColor = true;
            this.BtnExporter.Click += new System.EventHandler(this.BtnExporter_Click);
            // 
            // BtnAjouter
            // 
            this.BtnAjouter.Location = new System.Drawing.Point(47, 327);
            this.BtnAjouter.Name = "BtnAjouter";
            this.BtnAjouter.Size = new System.Drawing.Size(215, 44);
            this.BtnAjouter.TabIndex = 16;
            this.BtnAjouter.Text = "Ajouter";
            this.BtnAjouter.UseVisualStyleBackColor = true;
            this.BtnAjouter.Click += new System.EventHandler(this.BtnAjouter_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 410);
            this.Controls.Add(this.BtnAjouter);
            this.Controls.Add(this.BtnExporter);
            this.Controls.Add(this.BtnImporter);
            this.Controls.Add(this.listUser);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxTelphone);
            this.Controls.Add(this.textBoxEmail);
            this.Controls.Add(this.textBoxNom);
            this.Controls.Add(this.textBoxPrenom);
            this.Controls.Add(this.BtnInfo);
            this.Name = "Form1";
            this.Text = "Annuaire";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnInfo;
        private System.Windows.Forms.TextBox textBoxPrenom;
        private System.Windows.Forms.TextBox textBoxNom;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.TextBox TextBoxTelphone;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView listUser;
        private System.Windows.Forms.Button BtnImporter;
        private System.Windows.Forms.Button BtnExporter;
        private System.Windows.Forms.Button BtnAjouter;
        private System.Windows.Forms.ColumnHeader columnPrenon;
        private System.Windows.Forms.ColumnHeader columNom;
        private System.Windows.Forms.ColumnHeader columnEmail;
        private System.Windows.Forms.ColumnHeader columnTelephone;
    }
}

