﻿using System;
using System.IO;

namespace ExportLibrary
{
    public static class TestFichier
    {
        public static void TestDll()
        {
            Console.WriteLine("Affichage : DLL");
        }

        public static void InfoLecteur()
        {
            // DriveInfo fournit des informations sur les lecteurs d'une machine
            DriveInfo[] drive = DriveInfo.GetDrives();    // Liste les lecteurs d'une machine
            foreach (DriveInfo d in drive)
            {
                Console.WriteLine(d.Name);              // Nom du lecteur
                Console.WriteLine(d.VolumeLabel);       // Nom du volume
                Console.WriteLine(d.DriveFormat);       // système de fichiers du lecteur NTFS, FAT ...
                Console.WriteLine(d.TotalFreeSpace);    // espace disponible sur le lecteur
            }
        }

        public static void Repertoire(string pathDirectory)
        {
            // 
            if (Directory.Exists(pathDirectory))
            {
                string[] fileInfo = Directory.GetFiles(@"c:\Formations\TestIO\");   // Liste les fichiers
                foreach (string s in fileInfo)
                {
                    Console.WriteLine(s);
                }
                string[] dirInfo = Directory.GetDirectories(@"c:\Formations\TestIO\csharpe"); // Liste les dossiers
                foreach (string s in dirInfo)
                {
                    Console.WriteLine(s);
                }
            }
            else
            {
                Directory.CreateDirectory(pathDirectory);       // s'il n'existe pas on crée le répertoire 
            }
            if (!File.Exists(pathDirectory + @"\F1.bin"))
            {
                File.Create(pathDirectory + @"\F1.bin");         // s'il n'existe pas on crée le fichier
            }
        }

        public static void EcrireFichierText(string path)
        {   // Using => Équivalent d'un try / finally + Close()
            using (StreamWriter sw = new StreamWriter(path, true)) // StreamWriter Ecrire un fichier texte
            {                                                      // append à true "compléte" le fichier s'il existe déjà, à false le fichier est écrasé
                for (int i = 0; i < 10; i++)
                {
                    sw.WriteLine("Bonjour");    // WriteLine => Ecrire une ligne dans le fichier
                }
            }
        }

        public static void LireFichierTexte(String path)
        {   // sans utilisier using
            StreamReader sr = null;
            try
            {
                sr = new StreamReader(path); // StreamReader Lire un fichier texte
                while (!sr.EndOfStream)     // Propriété EndOfStream est vrai si le fichier atteint la fin du fichier
                {
                    Console.WriteLine(sr.ReadLine()); // ReadLine => Lire une ligne dans le fichier
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Le fichier {0} nexiste pas", path);
            }
            catch (Exception e)
            {
                Console.WriteLine("erreur lecture fichier: {0}", e.GetType());
            }
            finally
            {
                sr.Close();     // Fermer le flux
                sr.Dispose();   // Libérer les ressources
            }
        }

        public static void EcrireFichierBinaire(String path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))       // FileStream =>  permet de Lire/Ecrire un fichier binaire
            {
                for (byte i = 0; i < 100; i++)
                {
                    fs.WriteByte(i);            // Ecriture d'un octet dans le fichier
                }
            }
        }

        public static void LireFichierBinaire(String path)
        {
            byte[] tab = new byte[100];
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                //for (byte i = 0; i < 100; i++)
                //{ 
                //    Console.WriteLine(fs.ReadByte()); // // Lecture d'un octet dans le fichier
                //}

                fs.Read(tab, 0, 100);       // lecture de 100 octets dand le fichier, ils sont placés dans le tableau tab à partir de l'indice 0
                foreach (byte b in tab)
                {
                    Console.WriteLine(b);
                }

            }
        }

        //Exercice Fichier
        // Ecrire une méthode pour parcourir le système fichier à partir du chemin passé en paramètre
        //  et afficher le nom des fichiers et des dossiers 
        public static void ScanRep(string path)
        {
            Console.WriteLine("- Dossier {0}", path);
            string[] dirInfo = Directory.GetDirectories(path); // Liste les dossiers
            foreach (string pathRep in dirInfo)
            {
                ScanRep(pathRep);       // Parcours récursif des dossiers
            }
            string[] fileInfo = Directory.GetFiles(path);   // Liste les fichiers
            foreach (string pathFile in fileInfo)
            {
                Console.WriteLine("\t- Fichier {0}", pathFile);
            }

        }
    }
}
