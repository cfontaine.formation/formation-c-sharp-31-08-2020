﻿using System;
using System.Diagnostics;
using System.Threading;

namespace _14_Thread
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Debut");
            Thread t1 = new Thread(new ThreadStart(Job1));
            Thread t2 = new Thread(new ThreadStart(Job2));
            t1.Name = "T1";
            t2.Name = "T2";
            t1.Start();
            t2.Start();
            Console.WriteLine("Fin");

            //Processus
            Process p2 = Process.Start("Notepad.exe");

            Console.ReadKey();
        }

        private static void Job1()
        {
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine(Thread.CurrentThread.Name + " " + i);
            }

        }

        private static void Job2()
        {
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine(Thread.CurrentThread.Name + " " + i);
                Thread.Sleep(100);
            }

        }
    }

}

