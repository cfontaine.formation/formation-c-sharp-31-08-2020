﻿using System;
using System.Text;

namespace _05_methodes
{

    class Program
    {
        static void Main(string[] args)
        {
            // Appel de methode
            Console.WriteLine(Multiplication(2.8, 1.5));

            // Appel de methode (sans retour)
            Afficher("Bonjour");

            Console.WriteLine(TestMultiReturn(true, 1, 2));
            Console.WriteLine(TestMultiReturn(false, 1, 2));

            // Exercice méthode parité
            Console.WriteLine(Even(2));
            Console.WriteLine(Even(3));

            // Passage de paramètre

            // Passage par valeur
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode

            // Type valeur
            int param = 8;
            Console.WriteLine("Entrée param={0}", param);
            TestParamValeur(param);
            Console.WriteLine("Sortie param={0}", param);// Si la valeur est modifiée dans la méthode, cela n'a pas d'influence sur la variable param

            // Type référence
            StringBuilder sb = new StringBuilder("Valeur Init");
            TestParamValeurReference(sb);
            Console.WriteLine("Sortie param={0}", sb);

            // Passage par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            param = 8;
            Console.WriteLine("Entrée param={0}", param);
            TestParamReference(ref param);   // Il faut utiliser le mot clé ref pour la définition de méthode mais aussi lors de l'appel
            Console.WriteLine("Sortie param={0}", param);

            // Paramètre out
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée
            int sortie;
            TestParamOut(out sortie);   // Il faut utiliser le mot clé out pour la définition de méthode mais aussi lors de l'appel
            Console.WriteLine("Sortie param={0}", sortie);

            TestParamOut(out int sortie2);  // On peut de déclarer la variable de retour dans les arguments pendant l'appel de la fonction
            Console.WriteLine("Sortie param={0}", sortie2);

            // On peut ignorer un paramètre out en le nommant _
            TestParamOut(out _);

            // Paramètre optionnel
            TestParamOptionnel(10, "Bonjour", true);
            TestParamOptionnel(10, "Bonjour");
            TestParamOptionnel(10);
            // TestParamOptionnel(12, true); //erreur

            //  Paramètres nommés
            TestParamOptionnel(tst: true, a: 10, str: "Hello");
            TestParamOptionnel(a: 12, tst: true);

            // Nombre d'arguments variable
            Console.WriteLine(SommeParamVariable("test", 1, 5, 7));
            Console.WriteLine(SommeParamVariable("test", 1, 5, 7, 45, 68));
            Console.WriteLine(SommeParamVariable());

            // Surcharge de méthode
            Console.WriteLine(Somme(1, 2));
            Console.WriteLine(Somme(1, 2, 4));
            Console.WriteLine(Somme(4.0, 2.0));
            Console.WriteLine(Somme(10.0f, 2.0f));

            // Méthode récursive
            Console.WriteLine(Factorial(3));

            // Affichage des paramètres passés au programmes
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> options de démarrage -> Arguments de la ligne de commande 
            foreach (var s in args)
            {
                Console.WriteLine(s);
            }
            Console.WriteLine("\n\nAppuyer sur une touche pour fermer la console");
            Console.ReadKey();

            // Exercice: Methode Tableau 
            ExerciceTableau();
        }

        static double Multiplication(double d1, double d2)
        {
            return d1 * d2; // L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur (expression à droite)
        }

        static void Afficher(string str)    // void => pas de valeur retournée
        {
            Console.WriteLine(str);
            //   avec void => return; ou return peut être omis 
        }

        static int TestMultiReturn(bool addition, int a, int b)
        {
            if (addition)
            {
                return a + b;
            }
            else
            {
                return a - b;
            }
        }

        #region Exercice_Parite
        // Écrire une méthode even qui prend un entier en paramètre
        // Elle retourne vrai, si il est paire
        static bool Even(int val)
        {
            return val % 2 == 0;
        }

        //ou
        //static bool Even(int val)
        //{
        //    if (val % 2 == 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        // ou
        //static bool Even(int val)
        //{
        //    return val % 2 == 0 ? true : false;
        //}
        #endregion

        #region Methode_Passage_Parametre
        // Passage par valeur (type valeur)
        static void TestParamValeur(int x)
        {
            Console.WriteLine("Dans Méthode1 x={0}", x);
            x = 3;          // La modification  de la valeur du paramètre x n'a pas d'influence en dehors de la méthode
            Console.WriteLine("Dans Méthode2 x={0}", x);
        }

        // Passage par valeur (type référence)
        static void TestParamValeurReference(StringBuilder x)
        {
            Console.WriteLine("Dans Méthode1 x={0}", x);
            //  x = new StringBuilder("Valeur Méthode");    // Comme avec un type valeur, La modification du paramètre n'a pas de répercution en dehors de la méthode
            x.Append(" Ajout Méthode");                     // Par contre, on peut modifier l'état de l'objet qui a pour référence x 
            Console.WriteLine("Dans Méthode2 x={0}", x);
        }

        // Passage par référence => ref
        static void TestParamReference(ref int x)
        {   // x est une référence vers une variable, si on modifie sa valeur dans la méthode,  c'est rééllement le contenu de la variable qui est modifiée
            Console.WriteLine("Dans Méthode1 x={0}", x);
            x = 3;
            Console.WriteLine("Dans Méthode2 x={0}", x);
        }

        // Passage par référence avec out
        static void TestParamOut(out int x)
        {
            x = 3;  // La méthode doit obligatoirement affecter une valeur aux paramètres out
            Console.WriteLine("Dans Méthode2 x={0}", x);
        }

        // Pour les arguments passés par valeur, on peut leurs donner des valeurs par défaut
        static void TestParamOptionnel(int a, string str = "Default Value", bool tst = false)
        {
            Console.WriteLine("a={0} str={1} tst={2}", a, str, tst);
        }

        // Nombre d'arguments variable => params
        static int SommeParamVariable(string nom = "Default", params int[] vals)
        {
            Console.WriteLine(nom);
            int somme = 0;
            foreach (int v in vals)
            {
                somme += v;
            }
            return somme;
        }
        #endregion

        #region Exercice_Methode_Tableau
        // - Écrire un méthode qui affiche un tableau d’entier
        // - Écrire une méthode qui permet de saisir :
        //      - La taille du tableau
        //      - Les éléments du tableau
        // - Écrire une méthode qui calcule :
        //      - le minimum d’un tableau d’entier
        //      - le maximum
        //      - la moyenne
        // - Faire un menu qui permet de lancer ces méthodes

        static void AfficherTab(int[] tab)
        {
            Console.Write("[");
            foreach (int t in tab)
            {
                Console.Write(" {0}", t);
            }
            Console.WriteLine(" ]");
        }

        static int[] SaisieTab()
        {
            Console.Write("Entrer la taille du tableau ");
            int size = Convert.ToInt32(Console.ReadLine());

            int[] tabVal = new int[size];
            for (int i = 0; i < tabVal.Length; i++)
            {
                Console.Write("Valeur[{0}]=", i);
                tabVal[i] = Convert.ToInt32(Console.ReadLine());
            }
            return tabVal;
        }

        static void CalculTab(int[] tab, out int minimum, out int maximum, out double moyenne)
        {
            minimum = tab[0];
            maximum = tab[0];
            double somme = tab[0];
            for (int i = 1; i < tab.Length; i++)
            {
                if (tab[i] > maximum)
                {
                    maximum = tab[i];
                }
                if (tab[i] < minimum)
                {
                    minimum = tab[i];
                }
                somme += tab[i];
            }
            moyenne = somme / tab.Length;
        }

        static int ChoixMenu()
        {
            Console.WriteLine("1 - Saisir le tableau");
            Console.WriteLine("2 - Afficher le tableau");
            Console.WriteLine("3 - Afficher le minimum, le maximum et la moyenne");
            Console.WriteLine("0 - Quitter");
            Console.Write("Choix = ");
            return Convert.ToInt32(Console.ReadLine());
        }

        static void ExerciceTableau()
        {
            int[] tab = null;
            int choix;
            do
            {
                choix = ChoixMenu();
                switch (choix)
                {
                    case 1:
                        tab = SaisieTab();
                        if (tab.Length == 0)
                        {
                            tab = null;
                        }
                        break;
                    case 2:
                        if (tab != null)
                        {
                            AfficherTab(tab);
                        }
                        else
                        {
                            Console.WriteLine("Il n'y a pas de tableau saisie");
                        }
                        break;
                    case 3:
                        if (tab != null)
                        {
                            CalculTab(tab, out int minimum, out int maximum, out double moyenne);
                            Console.WriteLine("Minimum={0}  Maximum={1}  Moyenne={2}", minimum, maximum, moyenne);
                        }
                        else
                        {
                            Console.WriteLine("Il n'y a pas de tableau saisie");
                        }
                        break;
                }

            } while (choix != 0);

        }
        #endregion

        #region Surcharge_de_méthode

        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature

        static int Somme(int a, int b)
        {
            Console.WriteLine("Entier");
            return a + b;
        }

        static int Somme(int a, int b, int c)
        {
            Console.WriteLine("Entier3");
            return a + b + c;
        }
        static double Somme(double a, double b)
        {
            Console.WriteLine("Double");
            return a + b;
        }

        static float Somme(float f1, float f2)
        {
            Console.WriteLine("Float");
            return f1 + f2;
        }
        #endregion

        #region Recursivite
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1)// condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }
        #endregion
    }
}
