﻿using System;
using System.Text.RegularExpressions;

namespace _09_delegate
{
    class Program
    {
        // Déléguation => Définition de prototypes de fonctions
        public delegate int Operation(int n1, int n2);

        // Méthode correspondant au prototype Operation
        // Retourne un entier et a pour paramètre 2 entiers
        public static int Ajouter(int n1, int n2)
        {
            return n1 + n2;
        }

        public static int Multiplier(int n1, int n2)
        {
            return n1 * n2;
        }

        static void Main(string[] args)
        {
            // Appel d'un délégué C#
            // Operation op = new Operation(Ajouter);
            Operation op = new Operation(Multiplier);
            Console.WriteLine(op(1, 2));

            // On peut utiliser les délégués pour passer une méthode en paramètre d'une autre méthode
            AfficherCalcul(4, 5, new Operation(Ajouter));

            // Appel d'un délégué C# 2.0 => plus besoin de méthode correspondant au prototype
            Operation op2 = delegate (int n1, int n2) { return n1 + n2; };
            Operation op2s = delegate (int n1, int n2) { return n1 - n2; };
            AfficherCalcul(4, 5, op2);
            AfficherCalcul(4, 5, op2s);
            AfficherCalcul(4, 5, delegate (int n1, int n2) { return n1 * n2; });

            // // Appel d'un délégué C# 3.0 => utilisation des lambdas
            // Operation op3 = (int n1, int n2) => { return n1 + n2; };

            // Lambda =>méthode sans nom, les paramètres et le corps de la méthode sont séparés par l'opérateur =>
            // Syntaxe
            // () => expression     S'il n'y a pas de paramètre
            // 1 paramètre => expression
            // (paramètres) => expression
            // (paramètres) => { instructions }

            Operation op3 = (int n1, int n2) => n1 + n2;
            AfficherCalcul(4, 5, op3);
            AfficherCalcul(4, 5, (int n1, int n2) => n1 * n2);

            int[] tab = { 3, -4, 7, 6, -1, 0, 10 };
            SortTab(tab, (int n1, int n2) => n1 > n2);
            foreach (int a in tab)
            {
                Console.WriteLine(a);
            }
            Console.WriteLine(tab);

            // Expressions Régulières
            Regex reg = new Regex(@"^([\w\.\-] +)@([\w\-] +)((\.(\w){2, 3})+)$");
            Console.WriteLine(reg.IsMatch("azerty"));
            Console.WriteLine(reg.IsMatch("cfontaine@jehann.fr"));
            Console.WriteLine(Regex.IsMatch("cfontaine@jehann.fr", @"^([\w\.\-] +)@([\w\-] +)((\.(\w){2, 3})+)$"));
            Console.ReadKey();
        }

        static void AfficherCalcul(int a, int b, Operation op)
        {
            Console.WriteLine(op(a, b));
        }

        public delegate bool Comparaison(int n1, int n2);
        static void SortTab(int[] tab, Comparaison cmp)
        {
            bool notDone = true;
            int end = tab.Length - 1;
            while (notDone)
            {
                notDone = false;
                for (int i = 0; i < end; i++)
                {
                    if (cmp(tab[i], tab[i + 1]))
                    {
                        int tmp = tab[i];
                        tab[i] = tab[i + 1];
                        tab[i + 1] = tmp;
                        notDone = true;
                    }
                }
                end--;
            }
        }

    }
}
